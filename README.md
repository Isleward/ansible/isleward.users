# Ansible Role: users

* Sets up a user for maintenance of the server
* Authorizes ssh keys for the maintenance user
* Enables passwordless sudo for the maintenance user

## Requirements

  CentOS 7

## Role Variables

List of ssh keys to authorize for the user

```yml
ssh_public_keys:
  - ~/.ssh/id_rsa.pub
```

Name of the user

```yml
maintenance_user: isleward
```

## Dependencies

  None.

## Example Playbook

```yml
- hosts: localhost
  roles:
    - isleward.users
```  

## Author Information

This role was created in 2018 by Vildravn.